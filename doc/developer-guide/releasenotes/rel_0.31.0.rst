.. _rel-0.31.0:

Release 0.31.0
==============

Welcome to Zotonic 0.31.0, released on 2 August, 2017.

Main changes are:

* Upgraded Facebook API from version 2.3 to 2.9
* Add dropdown in the admin's connect dialog to filter on content group

Commits since 0.30.0
--------------------

There were 7 commits since release 0.30.0.

Big thanks to all people contributing to Zotonic!

Git shortlog
............

Marc Worrell (6):
      filewatcher: fix blacklist re for log files.
      mod_facebook: upgrade api to v2.9.
      mod_base: add optional class argument to overlay_open action. (#1757)
      Fix a problem with entering negative dates in the admin. Issue #1766
      mod_admin: add connect-dialog filter for content group. (#1768)
      mod_facebook: also request first and last_name from the api.

rl-king (1):
      Add missing punctuation (#1772)
