% Module untuk mem-filter bad words saat mengirimkan pesan
% Bad words pada pesan akan diganti dengan asterisk dengan jumlah karakter sama dengan panjang bad words untuk setiap bad words
% Kode ascii dari asterisk (*) adalah 42
% Implementasi semi-hardcode, tidak bisa menangani bad word yang disingkat
% Dalam tugas kali ini, bad words merupakan kata-kata acak yang dimasukkan ke dalam list yang ditentukan

-module(bad_word_filter).
-export([filter_sentence/1]).

filter_sentence(Str) ->
    List = splitCus(Str),
    NewList = filter_split(List),
    merge(NewList).

filter_split(List) -> filter_split(List,[]).
filter_split([],Acc) -> Acc;
filter_split([H|T],Acc) -> filter_split(T,Acc++[bad_word_filter(H)]).

splitCus(String) -> splitCus(String,[],[]).
splitCus([],Acc1,[]) -> Acc1;
splitCus([],Acc1,Acc2) -> Acc1++[Acc2];
splitCus([32|T],Acc,[]) -> splitCus(T,Acc,[]);
splitCus([32|T],Acc1,Acc2) -> splitCus(T,Acc1++[Acc2],[]);
splitCus([H|T],Acc1,Acc2) -> splitCus(T,Acc1,Acc2++[H]).

merge(List) -> merge(List,[],false).
merge([],Acc,_) -> Acc;
merge([H|T],Acc,false) -> merge(T,Acc++H,true);
merge([H|T],Acc,true) -> merge(T,Acc++" "++H,true).

bad_word_filter(Str) ->
	Bad_List = ["h3h3", 
				"ahahaha", 
				"asahahnwnnzx", 
				"xdxdasqjs",
				"ree",
				"reeee",
				"reeeeeeee",
				"hahahahehehehhahahah3",
				"reeeeeeeee",
				"fukovffff",
				"ora",
				"oraora",
				"oraoraora",
				"oraoraoraoraoraoraora",
				"muda",
				"mudamudamuda",
				"mudamudamudamudamuda",
				"wa",
				"shindeiru"],
	case contains(Bad_List,Str) of
	    true -> createAsterisk(length(Str));
	    false -> Str
	end.

createAsterisk(N) -> createAsterisk(N,[]).
createAsterisk(0,Acc) -> Acc;
createAsterisk(N,Acc) -> createAsterisk(N-1,[42|Acc]).

contains([],Str) -> false;
contains([H|T],Str) -> 
	case check(H,Str) of
		true -> true;
		false -> contains(T,Str)
	end.

check([],[]) -> true;
check([],_) -> false;
check(_,[]) -> false;
check([Ha|Ta],[Hb|Tb]) ->
	case Ha =:= Hb of
		true -> check(Ta,Tb);
		false -> false
	end.