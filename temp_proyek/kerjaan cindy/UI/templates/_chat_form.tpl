  {% wire id="chat-form" 
              type="submit" 
              postback={newmessage} 
              delegate="mod_chat" %}
  <script>
  toggleSubmitPhoto = function() {
        console.log("lalala");
  }
  </script>
    <div class="panel-footer">
        <a href="#">
            <img src="/lib/images/OpenCamera1.png" id="photo" width="40" height="40" align="left" data-toggle="modal" data-target="#photoModal" />
        </a>
        <!-- Modal -->
        <div class="modal fade" id="photoModal" role="dialog">
            <div class="modal-dialog">         
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Choose Photo</h4>
                    </div>
                    <form class="form-inline">
                        <div class="modal-body" align="center"> 
                                <input type="file" name="docToUpload" id="docToUpload" class="form-control"/>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" value="Send" class="btn btn-default" data-dismiss="modal"/>
                        </div>
                    </form>
                </div>             
            </div>
        </div>
        <div class="input-group">
            <form id="chat-form" method="post" action="postback">
                <input class="form-control input-sm text_area_input" type="text" name="chat" id="chat" />
                <input type="submit" id="submit_btn">
            </form>
            {% validate id="chat" type={presence} %}
            <span class="input-group-btn">
                <a href="#">
                    <img src="/lib/images/emoji.png" id="emoji" width="40" height="40" align="left" onclick="myFunction()"  data-toggle="modal" data-target="#emojiModal"/>
                </a>
            </span>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="emojiModal" role="dialog">
            <div class="modal-dialog">         
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Choose Emoji</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-responsive" id="emoji_table">
                            <tr align="center">
                                <td>
                                    <a href="#" data-dismiss="modal">
                                        <img src="/lib/images/smile.png" id="smile" width="40" height="40">
                                    </a>
                                </td>
                                <td>
                                    <a href="#" data-dismiss="modal">
                                        <img src="/lib/images/sad.png" id="sad" width="40" height="40">
                                    </a>
                                </td>
                                <td>
                                    <a href="#" data-dismiss="modal">
                                        <img src="/lib/images/lol.png" id="lol" width="40" height="40">
                                    </a>
                                </td>
                                <td>
                                    <a href="#" data-dismiss="modal">
                                        <img src="/lib/images/angry.png" id="angry" width="40" height="40">
                                    </a>
                                </td>
                                <td>
                                    <a href="#" data-dismiss="modal">
                                        <img src="/lib/images/kiss.png" id="kiss" width="40" height="40">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>             
            </div>
        </div>
    </div>
{% chat %}