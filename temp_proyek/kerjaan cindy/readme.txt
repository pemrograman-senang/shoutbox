How to use mod_chat:
1. Move mod_chat folder into zotonic/modules
2. Make your own site
3. Activate Module mod_chat
4. Select a site page
5. Add {% include "_chat_box.tpl" %} on your site page (freely in a place you want)
6. Add {% lib 
             "css/chat.css" %} 
   to add the css on your shoutbox
7. Go to your page and check the shoutbox 