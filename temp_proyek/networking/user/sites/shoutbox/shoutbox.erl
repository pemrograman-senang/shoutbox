%% @author VictimCrasher
%% @copyright 2017 VictimCrasher
%% Generated on 2017-12-05
%% @doc This site was based on the 'empty' skeleton.

%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(shoutbox).
-author("Pemrograman Senang").

-mod_title("Shoutbox").
-mod_description("Shoutbox untuk semua.").
-mod_prio(10).
%-mod_depends([
%        mod_mqtt
%    ]).

-include_lib("zotonic.hrl").
%-include_lib("emqtt/include/emqtt.hrl").

-export([event/2]).
%-export(['mqtt:~site'/3, observe_acl_is_allowed/2]).

%%====================================================================
%% MTQQ Methods
%%====================================================================

%'mqtt:~site'(Message, Pid, _Context) ->
%    gen_server:cast(Pid, {msg, Message}).

%% Allow everybody to publish
%observe_acl_is_allowed(#acl_is_allowed{object=#acl_mqtt{topic = <<"site/shoutbox">>}}, _Context) ->
%    true;
%observe_acl_is_allowed(_AclIsAllowed, _Context) ->
%    undefined.

%%====================================================================
%% Event
%%====================================================================

event(#submit{message={newmessage, []}}, Context) ->
	Chat = z_context:get_q(chat, Context),
	z_template:render_to_iolist("_chat_row.tpl", [{chat, Chat}, {name, "anonymous"}], Context),
	Context.


%%====================================================================
%% support functions go here
%%====================================================================

