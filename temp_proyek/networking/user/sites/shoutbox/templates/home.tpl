{% extends "base.tpl" %}

{% block title %}{{ m.site.title }}{% endblock %}

{% block main %}

{% include "_chat_box.tpl" %}

{% button class="btn btn-primary btn-lg" action={redirect dispatch=`admin`} text=_"Visit Admin Interface" %}
{% button class="btn btn-info btn-lg" action={redirect dispatch=`admin_edit_rsc` id=`page_home`} text=_"Edit this page" %}

{% endblock %}
