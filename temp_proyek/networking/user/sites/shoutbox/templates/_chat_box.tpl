<section id="chat-box" class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Shout It Out!</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> ShoutBox </span>
                </div>
                <div class="panel-body">
                    <div id="chat-list-div" class="zp-60" style="height: 180px; overflow: scroll; overflow-x: hidden;">
                        <ul id="chat-list" class="chat" style="list-style:none;">{% include "_chat_row.tpl" %}</ul>
                    </div>
                </div>
            {% include "_chat_form.tpl" %}
        </div>
    </div>
</section>
