  {% wire id="chat-form" 
              type="submit" 
              postback={newmessage} 
              delegate="mod_chat" %}
              
  <form id="chat-form" method="post" action="postback">
        <div class="panel-footer">
            <a href="#">
                <img src="/lib/images/OpenCamera1.png" id="photo" width="40" height="40" align="left" />
            </a>
            <div class="input-group">
                <input class="form-control input-sm text_area_input" type="text" name="chat" id="chat" />
                <input type="submit" id="submit_btn">
                {% validate id="chat" type={presence} %}
                <span class="input-group-btn">
                    <a href="#">
                        <img src="/lib/images/emoji.png" id="emoji" width="40" height="40" align="left" />
                    </a>
                </span>
            </div>
        </div>
  </form>
{% chat %}