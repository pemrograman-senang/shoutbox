<li>
    <span class="chat-img pull-left">
        <img src="{{ image_url|default:"/lib/images/anon.jpg" }}" alt="User Avatar" width="40" height="40" class="img-circle" />
    </span>
    <div class="chat-body clearfix">
        <div class="header">
            <strong class="primary-font">{{ name }}</strong> 
                <small class="text-muted pull-right">
                    <span class="glyphicon glyphicon-time">
                    </span>
                    {{ time_chat }}
                </small>
        </div>
        <p>
            {{ chat|escape }}
        </p>
    </div>
</li>