%% @author Marc Worrell <marc@worrell.nl>
%% @copyright 2015 Marc Worrell
%% @doc Chitchat model - store recent history in the database.

%% Copyright 2015 Marc Worrell
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.


%% Edited by Pemrograman Senang

-module(ehe_models).

-export([
    insert/3,
    list_rooms/1,
    list_messages/1,
    list_messages/2,
    prune/2,
    install/1
    ]).

insert(#shoutbox_msg{username=Username, time_chat=Timestamp, chat_message=Chat_message}, Context) ->
    Username1 = z_string:truncate(Username, 80),
    z_db:q("
        insert into shoutbox (username, time_chat, chat_message)
        values ($1, $2, $3)",
        [Username, Timestamp, Chat_message],
        Context).

list_messages(Context) ->
    Rs = z_db:q("select username,time_chat,chat_message
                 from shoutbox
                 order by created",
                Context),
    lists:map(fun({Username, Time_chat, Chat_message}) ->
                Timestamp = z_datetime:datetime_to_timestamp(Time_chat),
                #shoutbox_msg{username=Username, time_chat=Timestamp, chat_message=Chat_message}
              end,
              Rs).

list_messages(Room, Context) ->
    z_db:q("select name, message, created from chitchat where room = $1 order by created",
           [Room],
           Context).

prune(Hours, Context) ->
    z_db:q("delete from shoutbox 
            where created < now() - interval '"++integer_to_list(Hours)++" hours'",
           Context).

install(Context) ->
    case z_db:table_exists(shoutbox, Context) of
        false ->
            [] = z_db:q("
                create table shoutbox (
                    id serial not null,
                    username character varying (100) not null,
                    time_chat timestamp with time zone not null default current_timestamp,
                    chat_message text not null,

                    primary key (id)
                )
                ", Context),
            [] = z_db:q("create index shoutbox_time on chitchat(time_chat)", Context),
            z_db:flush(Context),
            ok;
        true ->
            ok
    end.
