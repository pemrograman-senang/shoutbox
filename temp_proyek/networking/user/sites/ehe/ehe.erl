%% @author VictimCrasher
%% @copyright 2017 VictimCrasher
%% Generated on 2017-12-02
%% @doc This site was based on the 'empty' skeleton.

%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ehe).
-author("Pemrograman Senang").

-mod_title("Shoutbox").
-mod_description("Shoutbox").
-mod_prio(10).

-include_lib("zotonic.hrl").
-include_lib("emqtt/include/emqtt.hrl").

-record(chat_records, {
		username,
		time_chat,
		msg
	}).

%% Keep the last 200 messages in a room
-define(CHAT_HISTORY, 200).

%% Keep inactive chat for at most a month
-define(CHAT_ANCIENT, 30*24).

%% MQTT subscriptions
-export([
    'mqtt:~site/chitchat/msg/+'/3,
    'mqtt:~site/chitchat/status'/3,

    event/2,

    observe_acl_is_allowed/2,
    observe_tick_1h/2,

    online/1,
    %rooms/1,
    messages/2,

    manage_schema/2
]).

%% Listen to the chit-chatting
'mqtt:~site/chitchat/msg/+'(Message, Pid, _Context) ->
    gen_server:cast(Pid, {msg, Message}).

'mqtt:~site/chitchat/status'(Message, Pid, _Context) ->
    gen_server:cast(Pid, {status, Message}).

%% Allow everybody to publish
observe_acl_is_allowed(#acl_is_allowed{object=#acl_mqtt{ words=[<<"site">>, _, <<"chitchat">>|_] }}, _Context) ->
    true;
observe_acl_is_allowed(_AclIsAllowed, _Context) ->
    undefined.

%% @doc Return the list of rooms
online(Context) ->
    Name = z_utils:name_for_host(?MODULE, Context),
    gen_server:call(Name, online).

%% @doc Return the messages in a room
messages(Room, Context) ->
    Name = z_utils:name_for_host(?MODULE, Context),
    gen_server:call(Name, {msg, z_convert:to_binary(Room)}).

manage_schema(_Version, Context) ->
    ehe_models:install(Context),
    ok.

%%====================================================================
%% support functions go here
%%====================================================================

