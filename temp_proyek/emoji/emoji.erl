% cd("C:/Users/JEFFRY/Desktop/Pemfung/Project").
% https://apps.timwhitlock.info/emoji/tables/unicode
% only 6a (Emoji Face)

% -module(helloworld).
-module(emoji).
-compile([export_all]).

start() ->
    
    Dict = dict:from_list(
        [{":grinning:",  ["F0","9F","98","81"]}, 
        {":innocent:",  ["F0","9F","98","87"]},
        {":smiling_imp:",  ["F0","9F","98","88"]},
        {":sunglasses:",  ["F0","9F","98","8E"]},
        {":neutral_face:",  ["F0","9F","98","90"]},
        {":expressionless:",  ["F0","9F","98","91"]},
        {":confused:",  ["F0","9F","98","95"]},
        {":kissing:",  ["F0","9F","98","97"]},
        {":kissing_smiling_eyes:",  ["F0","9F","98","99"]},
        {":stuck_out_tongue:",  ["F0","9F","98","9B"]},
        {":worried:",  ["F0","9F","98","9F"]},
        {":frowning:",  ["F0","9F","98","A6"]},
        {":anguished:",  ["F0","9F","98","A7"]},
        {":grimacing:",  ["F0","9F","98","AC"]},
        {":open_mouth:",  ["F0","9F","98","AE"]},
        {":hushed:",  ["F0","9F","98","AF"]},
        {":sleeping:",  ["F0","9F","98","B4"]},
        {":no_mouth:",  ["F0","9F","98","B6"]}
        ]),
    
    %List_Emoji
    dict:map(
    	fun(K,V) -> 
    		io:format("~p~n", [K]) end,
    	 Dict),
    
    % misal ada input ":no_mouth"	 
    try dict:fetch(":no_mouth:", Dict) of
		_ -> 
		Hexs = dict:fetch(":no_mouth:", Dict),
        Ints = [list_to_integer(Hex,16) || Hex <- Hexs],
    
        Binary = list_to_binary(Ints),
        io:fwrite(binary_to_list(Binary))
	catch
		_:_ ->
		    io:fwrite("Emoji Not Found")
    end.